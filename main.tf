provider "aws" {
  region = "us-west-2"
}

resource "aws_instance" "example" {
  ami           = "ami-02d8bad0a1da4b6fd"
  instance_type = "t2.micro"

  tags = {
    Name        = "MyEC2Instance"
    Environment = "Production"
  }
}